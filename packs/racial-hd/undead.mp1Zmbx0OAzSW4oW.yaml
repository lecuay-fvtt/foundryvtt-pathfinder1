_id: mp1Zmbx0OAzSW4oW
_key: '!items!mp1Zmbx0OAzSW4oW'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/affliction_07.jpg
name: Undead
system:
  bab: med
  classSkills:
    clm: true
    dis: true
    fly: true
    int: true
    kar: true
    kre: true
    per: true
    sen: true
    spl: true
    ste: true
  description:
    value: >-
      <p>Undead are once-living creatures animated by spiritual or supernatural
      forces.</p>

      <h2>Features</h2>

      <p>An undead creature has the following features.</p>

      <ul>

      <li>d8 Hit Die.</li>

      <li>Base attack bonus equal to 3/4 total Hit Dice (medium
      progression).</li>

      <li>Good <em>Will</em> Saves.</li>

      <li>Skill points equal to 4 + Int modifier (minimum 1) per Hit Die. Many
      undead, however, are mindless and gain no skill points or feats. The
      following are class skills for undead: <em>Climb</em>, <em>Disguise</em>,
      <em>Fly</em>, <em>Intimidate</em>, <em>Knowledge</em> (arcane),
      <em>Knowledge</em> (religion), <em>Perception</em>, <em>Sense Motive</em>,
      <em>Spellcraft</em>, and <em>Stealth</em>.</li>

      </ul>

      <h2>Traits</h2>

      <p>An undead creature possesses the following traits (unless otherwise
      noted in a creature’s entry).</p>

      <ul>

      <li>No Constitution score. Undead use their <em>Charisma</em> score in
      place of their Constitution score when calculating hit points,
      <em>Fortitude</em> saves, and any special ability that relies on
      Constitution (such as when calculating a breath weapon’s DC).</li>

      <li><em>Darkvision</em> 60 feet.</li>

      <li>Immunity to all mind-affecting effects (charms, compulsions, morale
      effects, patterns, and phantasms).</li>

      <li>Immunity to death effects, disease, paralysis, poison, sleep effects,
      and stunning.</li>

      <li>Not subject to nonlethal damage, <em>ability drain</em>, or energy
      drain. Immune to <em>damage</em> to its physical ability scores
      (Constitution, Dexterity, and <em>Strength</em>), as well as to exhaustion
      and fatigue effects.</li>

      <li>Cannot heal damage on its own if it has no <em>Intelligence</em>
      score, although it can be healed. Negative energy (such as an inflict
      spell) can heal undead creatures. The fast healing special quality works
      regardless of the creature’s <em>Intelligence</em> score.</li>

      <li>Immunity to any effect that requires a <em>Fortitude</em> save (unless
      the effect also works on objects or is harmless).</li>

      <li>Not at risk of death from massive damage, but is immediately destroyed
      when reduced to 0 hit points.</li>

      <li>Not affected by <em>raise dead</em> and <em>reincarnate</em> spells or
      abilities. Resurrection and <em>true resurrection</em> can affect undead
      creatures. These spells turn undead creatures back into the living
      creatures they were before becoming undead.</li>

      <li>Proficient with its natural weapons, all simple weapons, and any
      weapons mentioned in its entry.</li>

      <li>Proficient with whatever type of armor (light, medium, or heavy) it is
      described as wearing, as well as all lighter types. Undead not indicated
      as wearing armor are not proficient with armor. Undead are proficient with
      shields if they are proficient with any form of armor.</li>

      <li>Undead do not breathe, eat, or sleep.</li>

      </ul>
  links:
    supplements:
      - uuid: Compendium.pf1.monster-abilities.Item.abPQPu23CmkfWudq
      - uuid: Compendium.pf1.monster-abilities.Item.kcCRPAHsrWouTbij
  savingThrows:
    will:
      value: high
  skillsPerLevel: 4
  subType: racial
  tag: undead
type: class
