_id: qFh0gRGK3cjczkiR
_key: '!actors!qFh0gRGK3cjczkiR'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/red_28.jpg
items:
  - _id: wPinYpTszEQlBdwQ
    _key: '!actors.items!qFh0gRGK3cjczkiR.wPinYpTszEQlBdwQ'
    _stats:
      compendiumSource: Compendium.pf1.racialhd.Item.mp1Zmbx0OAzSW4oW
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/affliction_07.jpg
    name: Undead
    system:
      bab: med
      classSkills:
        clm: true
        dis: true
        fly: true
        int: true
        kar: true
        kre: true
        per: true
        sen: true
        spl: true
        ste: true
      description:
        value: >-
          <p>Undead are once-living creatures animated by spiritual or
          supernatural forces.</p>

          <h2>Features</h2>

          <p>An undead creature has the following features.</p>

          <ul>

          <li>d8 Hit Die.</li>

          <li>Base attack bonus equal to 3/4 total Hit Dice (medium
          progression).</li>

          <li>Good <em>Will</em> Saves.</li>

          <li>Skill points equal to 4 + Int modifier (minimum 1) per Hit Die.
          Many undead, however, are mindless and gain no skill points or feats.
          The following are class skills for undead: <em>Climb</em>,
          <em>Disguise</em>, <em>Fly</em>, <em>Intimidate</em>,
          <em>Knowledge</em> (arcane), <em>Knowledge</em> (religion),
          <em>Perception</em>, <em>Sense Motive</em>, <em>Spellcraft</em>, and
          <em>Stealth</em>.</li>

          </ul>

          <h2>Traits</h2>

          <p>An undead creature possesses the following traits (unless otherwise
          noted in a creature’s entry).</p>

          <ul>

          <li>No Constitution score. Undead use their <em>Charisma</em> score in
          place of their Constitution score when calculating hit points,
          <em>Fortitude</em> saves, and any special ability that relies on
          Constitution (such as when calculating a breath weapon’s DC).</li>

          <li><em>Darkvision</em> 60 feet.</li>

          <li>Immunity to all mind-affecting effects (charms, compulsions,
          morale effects, patterns, and phantasms).</li>

          <li>Immunity to death effects, disease, paralysis, poison, sleep
          effects, and stunning.</li>

          <li>Not subject to nonlethal damage, <em>ability drain</em>, or energy
          drain. Immune to <em>damage</em> to its physical ability scores
          (Constitution, Dexterity, and <em>Strength</em>), as well as to
          exhaustion and fatigue effects.</li>

          <li>Cannot heal damage on its own if it has no <em>Intelligence</em>
          score, although it can be healed. Negative energy (such as an inflict
          spell) can heal undead creatures. The fast healing special quality
          works regardless of the creature’s <em>Intelligence</em> score.</li>

          <li>Immunity to any effect that requires a <em>Fortitude</em> save
          (unless the effect also works on objects or is harmless).</li>

          <li>Not at risk of death from massive damage, but is immediately
          destroyed when reduced to 0 hit points.</li>

          <li>Not affected by <em>raise dead</em> and <em>reincarnate</em>
          spells or abilities. Resurrection and <em>true resurrection</em> can
          affect undead creatures. These spells turn undead creatures back into
          the living creatures they were before becoming undead.</li>

          <li>Proficient with its natural weapons, all simple weapons, and any
          weapons mentioned in its entry.</li>

          <li>Proficient with whatever type of armor (light, medium, or heavy)
          it is described as wearing, as well as all lighter types. Undead not
          indicated as wearing armor are not proficient with armor. Undead are
          proficient with shields if they are proficient with any form of
          armor.</li>

          <li>Undead do not breathe, eat, or sleep.</li>

          </ul>
      hp: 4
      savingThrows:
        will:
          value: high
      skillsPerLevel: 4
      subType: racial
      tag: undead
    type: class
  - _id: vHS3gH5Ipq5h8Lcj
    _key: '!actors.items!qFh0gRGK3cjczkiR.vHS3gH5Ipq5h8Lcj'
    _stats:
      compendiumSource: Compendium.pf1.weapons-and-ammo.Item.SjYkpvZqfgvh0EAd
      coreVersion: '12.331'
    img: systems/pf1/icons/items/weapons/scimitar.PNG
    name: Scimitar
    system:
      actions:
        - _id: b8tvl2f6459mbgeg
          ability:
            attack: str
            critRange: 18
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          damage:
            parts:
              - formula: sizeRoll(1, 6, @size)
                types:
                  - slashing
          duration:
            units: inst
          extraAttacks:
            type: standard
          name: Attack
          range:
            units: melee
            value: '0'
      baseTypes:
        - Scimitar
      broken: true
      description:
        value: >-
          <p>This curved sword is shorter than a longsword and longer than a
          shortsword. Only the outer edge is sharp, and the back is flat, giving
          the blade a triangular cross-section.</p>
      hardness: 10
      hp:
        base: 5
      links:
        children:
          - uuid: .Item.9HY0xVDApcd9aIAt
      material:
        base:
          value: steel
      price: 15
      showInQuickbar: false
      sources:
        - id: PZO1110
          pages: '142'
        - id: PZO1123
          pages: '18'
      subType: martial
      weaponGroups:
        - bladesHeavy
      weaponSubtype: 1h
      weight:
        value: 4
    type: weapon
  - _id: q12QC6j8YhHQKkSV
    _key: '!actors.items!qFh0gRGK3cjczkiR.q12QC6j8YhHQKkSV'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.MZnbCVzqpvPsMvfT
      coreVersion: '12.331'
    img: icons/commodities/claws/claw-bear-brown-grey.webp
    name: Claw
    system:
      actions:
        - _id: 4lo88d47gxkw8wts
          ability:
            attack: str
            damage: str
            damageMult: 1
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackName: Claw
          damage:
            parts:
              - formula: sizeRoll(1, 4, @size)
                types:
                  - bludgeoning
                  - slashing
          extraAttacks:
            formula:
              count: '1'
              label: 'Claw #{0}'
            type: custom
          name: Primary
          range:
            units: melee
        - _id: 7gySqfue8DjEdFFm
          ability:
            attack: str
            damage: str
            damageMult: 1
          actionType: mwak
          activation:
            type: nonaction
            unchained:
              type: nonaction
          attackName: Claw
          damage:
            parts:
              - formula: sizeRoll(1, 4, @size)
                types:
                  - bludgeoning
                  - slashing
          name: Secondary
          naturalAttack:
            primary: false
      baseTypes:
        - Claw
      description:
        value: >-
          <p>A primary natural weapon using your claws on your forelimbs or
          arms.</p><p>It deals bludgeoning and slashing damage, whichever is
          most beneficial.</p><hr /><p>For general rules on natural attacks,
          see:
          @UUID[Compendium.pf1.pf1e-rules.nBnUY0koBzXqoEft.JournalEntryPage.8zivKFrhTVkbtjm3]{Natural
          Attacks} (UMR).</p>
      proficient: true
      subType: natural
      weaponGroups:
        - natural
    type: attack
  - _id: aUtIMBgznvl8uwKw
    _key: '!actors.items!qFh0gRGK3cjczkiR.aUtIMBgznvl8uwKw'
    _stats:
      compendiumSource: Compendium.pf1.feats.Item.Uuiu3p982omhMEPj
      coreVersion: '12.331'
    img: systems/pf1/icons/feats/improved-initiative.jpg
    name: Improved Initiative
    system:
      changes:
        - _id: li3V0tV0
          formula: '4'
          target: init
          type: untyped
      description:
        value: >-
          <p><em>Your quick reflexes allow you to react rapidly to
          danger.</em></p><p><strong>Benefits</strong>: You get a +4 bonus on
          initiative checks.</p>
      sources:
        - id: PZO1110
          pages: 115, 127
      tags:
        - Combat
    type: feat
  - _id: NYpLpWjRjYghVENZ
    _key: '!actors.items!qFh0gRGK3cjczkiR.NYpLpWjRjYghVENZ'
    _stats:
      compendiumSource: Compendium.pf1.armors-and-shields.Item.iGu21zy6QtroUZ4O
      coreVersion: '12.331'
    img: systems/pf1/icons/items/armor/chain-shirt.png
    name: Chain Shirt
    system:
      armor:
        acp: 2
        dex: 4
        material:
          base:
            value: leather
        value: 4
      baseTypes:
        - Chain Shirt
      broken: true
      description:
        value: >-
          <p>Covering the torso, this shirt is made up of thousands of
          interlocking metal rings.</p>
      hardness: 10
      hp:
        base: 20
      price: 100
      slot: armor
      sources:
        - id: PZO1110
          pages: 150, 151
      spellFailure: 20
      weight:
        value: 25
    type: equipment
  - _id: 8LTAFnADocXMBigg
    _key: '!actors.items!qFh0gRGK3cjczkiR.8LTAFnADocXMBigg'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.abPQPu23CmkfWudq
      coreVersion: '12.331'
    img: icons/creatures/eyes/humanoid-single-blind.webp
    name: Darkvision
    system:
      abilityType: ex
      description:
        value: >-
          <p>A creature with darkvision can see in total darkness, usually to a
          range of 60 feet. Within this range, the creature can see as clearly
          as a sighted creature could see in an area of bright light. Darkvision
          is black and white only but otherwise like normal
          sight.</p><p><em>Format:</em> darkvision 60
          ft.</p><p><em>Location:</em> Senses.</p>
      sources:
        - id: PZO1133
          pages: '292'
        - id: PZO1127
          pages: '292'
        - id: PZO1137
          pages: '292'
      subType: misc
    type: feat
  - _id: RRqu0IdXbSr8Vp7t
    _key: '!actors.items!qFh0gRGK3cjczkiR.RRqu0IdXbSr8Vp7t'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.kcCRPAHsrWouTbij
      coreVersion: '12.331'
    img: icons/magic/death/hand-undead-skeleton-fire-green.webp
    name: Undead Traits
    system:
      abilityType: ex
      description:
        value: >-
          <p>Undead are immune to death effects, disease, mind-affecting effects
          (charms, compulsions, morale effects, patterns, and phantasms),
          paralysis, poison, sleep, stun, and any effect that requires a
          Fortitude save (unless the effect also works on objects or is
          harmless). Undead are not subject to ability drain, energy drain, or
          nonlethal damage. Undead are immune to damage or penalties to their
          physical ability scores (Strength, Dexterity, and Constitution), as
          well as to fatigue and exhaustion effects. Undead are not at risk of
          death from massive damage.</p><p><em>Format:</em> undead
          traits</p><p><em>Location:</em> Immune.</p>
      sources:
        - id: PZO1112
          pages: '305'
        - id: PZO1116
          pages: '303'
        - id: PZO1120
          pages: '300'
        - id: PZO1127
          pages: '301'
        - id: PZO1133
          pages: '301'
        - id: PZO1137
          pages: '300'
      subType: misc
    type: feat
  - _id: 9HY0xVDApcd9aIAt
    _key: '!actors.items!qFh0gRGK3cjczkiR.9HY0xVDApcd9aIAt'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/items/weapons/scimitar.PNG
    name: Scimitar
    system:
      actions:
        - _id: 4wAj0ktEIiSyIiWM
          ability:
            attack: str
            critRange: 18
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          damage:
            parts:
              - formula: sizeRoll(1, 6, @size)
                types:
                  - slashing
          duration:
            units: inst
          extraAttacks:
            type: standard
          name: Attack
          range:
            units: melee
            value: '0'
      attackNotes:
        - '@Use[Claw#Secondary;speaker]'
      baseTypes:
        - Scimitar
      broken: true
      proficient: true
      subType: weapon
      weapon:
        category: simple
        type: light
      weaponGroups:
        - bladesHeavy
    type: attack
name: Human Skeleton
prototypeToken:
  sight:
    enabled: true
  texture:
    src: systems/pf1/icons/skills/red_28.jpg
system:
  abilities:
    con:
      value: null
    dex:
      value: 14
    int:
      value: null
    str:
      value: 15
  attributes:
    naturalAC: 2
  details:
    alignment: ne
    biography:
      value: >-
        <p>Skeletons are the animated bones of the dead, brought to unlife
        through foul magic. While most skeletons are mindless automatons, they
        still possess an evil cunning imparted to them by their animating
        force—a cunning that allows them to wield weapons and wear armor.</p>
    bonusFeatFormula: '1'
    cr:
      base: 0.3375
    notes:
      value: <p><strong>Source</strong> <em>Pathfinder RPG Bestiary pg. 250</em></p>
  traits:
    ci:
      - deathEffects
      - disease
      - exhausted
      - fatigue
      - mindAffecting
      - paralyze
      - poison
      - sleep
      - stun
    di:
      - cold
      - nonlethal
    dr:
      value:
        - amount: 5
          operator: true
          types:
            - bludgeoning
            - ''
    senses:
      dv:
        value: 60
type: npc
