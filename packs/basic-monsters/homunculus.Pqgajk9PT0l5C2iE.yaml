_id: Pqgajk9PT0l5C2iE
_key: '!actors!Pqgajk9PT0l5C2iE'
_stats:
  coreVersion: '12.331'
img: icons/creatures/unholy/demon-hairy-winged-pink.webp
items:
  - _id: NvOfRROzG8g2GhOj
    _key: '!actors.items!Pqgajk9PT0l5C2iE.NvOfRROzG8g2GhOj'
    _stats:
      compendiumSource: Compendium.pf1.racialhd.Item.H8FbMUps5Z0gQdvV
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/mech_7.jpg
    name: Construct
    system:
      bab: high
      changes:
        - _id: 7bMee6VB
          formula: (max(0, @size - 3) * 10) + (max(0, @size - 6) * 10)
          target: mhp
          type: untypedPerm
      description:
        value: >-
          <p>A construct is an animated object or artificially created
          creature.</p>

          <h2>Features</h2>

          <p>A construct has the following features.</p>

          <ul>

          <li>d10 Hit Die.</li>

          <li>Base attack bonus equal to total Hit Dice (fast progression).</li>

          <li>No good saving throws.</li>

          <li>Skill points equal to 2 + Int modifier (minimum 1) per Hit Die.
          However, most constructs are mindless and gain no skill points or
          feats. Constructs do not have any class skills, regardless of their
          <em>Intelligence</em> scores.</li>

          <li>Construct Size Bonus Hit Points Fine — Diminutive — Tiny — Small
          10 Medium 20 Large 30 Huge 40 Gargantuan 60 Colossal 80</li>

          </ul>

          <h2>Traits</h2>

          <p>A construct possesses the following traits (unless otherwise noted
          in a creature’s entry).</p>

          <ul>

          <li>No Constitution score. Any DCs or other statistics that rely on a
          Constitution score treat a construct as having a score of 10 (no bonus
          or penalty).</li>

          <li>Low-light vision.</li>

          <li>Darkvision 60 feet.</li>

          <li>Immunity to all mind-affecting effects (charms, compulsions,
          morale effects, patterns, and phantasms).</li>

          <li>Immunity to disease, death effects, necromancy effects, paralysis,
          poison, sleep effects, and stunning.</li>

          <li>Cannot heal damage on its own, but often can be repaired via
          exposure to a certain kind of effect (see the creature’s description
          for details) or through the use of the <em>Craft Construct</em> feat.
          Constructs can also be healed through spells such as <em>make
          whole</em>. A construct with the fast healing special quality still
          benefits from that quality.</li>

          <li>Not subject to <em>ability damage</em>, <em>ability drain</em>,
          fatigue, exhaustion, energy drain, or nonlethal damage.</li>

          <li>Immunity to any effect that requires a <em>Fortitude</em> save
          (unless the effect also works on objects, or is harmless).</li>

          <li>Not at risk of death from massive damage. Immediately destroyed
          when reduced to 0 hit points or less.</li>

          <li>A construct cannot be raised or resurrected.</li>

          <li>A construct is hard to destroy, and gains bonus hit points based
          on size, as shown on the following table.</li>

          </ul>

          <ul>

          <li>Proficient with its natural weapons only, unless generally
          humanoid in form, in which case proficient with any weapon mentioned
          in its entry.</li>

          <li>Proficient with no armor.</li>

          <li>Constructs do not breathe, eat, or sleep.</li>

          </ul>

          <h2>Construct Size Bonus Hit Points</h2>

          <table border="1">

          <tbody>

          <tr>

          <td>Fine</td>

          <td>10</td>

          </tr>

          <tr>

          <td>Diminutive</td>

          <td>10</td>

          </tr>

          <tr>

          <td>Tiny</td>

          <td>10</td>

          </tr>

          <tr>

          <td>Small</td>

          <td>10</td>

          </tr>

          <tr>

          <td>Medium</td>

          <td>20</td>

          </tr>

          <tr>

          <td>Large</td>

          <td>30</td>

          </tr>

          <tr>

          <td>Huge</td>

          <td>40</td>

          </tr>

          <tr>

          <td>Gargantuan</td>

          <td>60</td>

          </tr>

          <tr>

          <td>Colossal</td>

          <td>80</td>

          </tr>

          </tbody>

          </table>
      hd: 10
      hp: 11
      level: 2
      skillsPerLevel: 2
      subType: racial
      tag: construct
    type: class
  - _id: T2giFcEScuuYky5N
    _key: '!actors.items!Pqgajk9PT0l5C2iE.T2giFcEScuuYky5N'
    _stats:
      compendiumSource: Compendium.pf1.feats.Item.0bZf3SDkvVOe2ujH
      coreVersion: '12.331'
    img: systems/pf1/icons/feats/lighting-reflexes.jpg
    name: Lightning Reflexes
    system:
      changes:
        - _id: rfDyTF8z
          formula: '2'
          target: ref
          type: untyped
      description:
        value: >-
          <p><em>You have faster reflexes than
          normal.</em></p><p><strong>Benefits</strong>: You get a +2 bonus on
          all Reflex saving throws.</p>
      sources:
        - id: PZO1110
          pages: 115, 130
      tags:
        - General
        - Saving Throw
    type: feat
  - _id: bOZ2ntYUb9gRFGy7
    _key: '!actors.items!Pqgajk9PT0l5C2iE.bOZ2ntYUb9gRFGy7'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.szjeStouwI3F3WdP
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/red_29.jpg
    name: Bite
    system:
      actions:
        - _id: jxtofrfc2lcw0osl
          ability:
            attack: str
            damage: str
            damageMult: 1.5
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          damage:
            parts:
              - formula: sizeRoll(1, 4, @size, 2)
                types:
                  - piercing
                  - bludgeoning
                  - slashing
          img: systems/pf1/icons/skills/red_29.jpg
          name: Attack
          range:
            units: melee
          tag: bite
      attackNotes:
        - plus @Use[Poison]
      baseTypes:
        - Bite
      description:
        value: >-
          <p>A primary natural weapon that crushes, tears and tears with teeth
          and jaws.</p><p>It deals bludgeoning, slashing or piercing damage,
          whichever is most beneficial one.</p><hr /><p>For general rules on
          natural attacks, see:
          @UUID[Compendium.pf1.pf1e-rules.nBnUY0koBzXqoEft.JournalEntryPage.8zivKFrhTVkbtjm3]{Natural
          Attacks} (UMR).</p>
      held: null
      proficient: true
      subType: natural
      weaponGroups:
        - natural
    type: attack
  - _id: dLRQJq2axyYKlPR3
    _key: '!actors.items!Pqgajk9PT0l5C2iE.dLRQJq2axyYKlPR3'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/spells/link-blue-1.jpg
    name: Telepathic Link
    system:
      abilityType: su
      description:
        value: >-
          <p>A homunculus cannot speak, but the process of creating one links it
          telepathically with its creator. A homunculus knows what its master
          knows and can convey to him or her everything it sees and hears, out
          to a distance of 1,500 feet.</p>
      languages:
        - Telepathic link
      sources:
        - id: PZO1112
          pages: '176'
      subType: misc
    type: feat
  - _id: HZ5AOwHntCPvlur0
    _key: '!actors.items!Pqgajk9PT0l5C2iE.HZ5AOwHntCPvlur0'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.tM956KLS078oZF72
      coreVersion: '12.331'
    img: systems/pf1/icons/races/android.png
    name: Construct Traits
    system:
      abilityType: ex
      description:
        value: >-
          <p>Constructs are immune to death effects, disease, mind-affecting
          effects (charms, compulsions, morale effects, patterns, and
          phantasms), necromancy effects, paralysis, poison, sleep, stun, and
          any effect that requires a Fortitude save (unless the effect also
          works on objects or is harmless). Constructs are not subject to
          ability damage, ability drain, energy drain, exhaustion, fatigue, or
          nonlethal damage. Constructs are not at risk of death from taking
          massive damage.</p><p><em>Format:</em> construct
          traits</p><p><em>Location:</em> Immune.</p>
      sources:
        - id: PZO1112
          pages: '299'
        - id: PZO1116
          pages: '295'
        - id: PZO1120
          pages: '293'
        - id: PZO1127
          pages: '291'
        - id: PZO1133
          pages: '291'
        - id: PZO1137
          pages: '291'
      subType: misc
    type: feat
  - _id: wMmqDxajo7H0ZCTT
    _key: '!actors.items!Pqgajk9PT0l5C2iE.wMmqDxajo7H0ZCTT'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.abPQPu23CmkfWudq
      coreVersion: '12.331'
    img: icons/creatures/eyes/humanoid-single-blind.webp
    name: Darkvision
    system:
      abilityType: ex
      description:
        value: >-
          <p>A creature with darkvision can see in total darkness, usually to a
          range of 60 feet. Within this range, the creature can see as clearly
          as a sighted creature could see in an area of bright light. Darkvision
          is black and white only but otherwise like normal
          sight.</p><p><em>Format:</em> darkvision 60
          ft.</p><p><em>Location:</em> Senses.</p>
      sources:
        - id: PZO1133
          pages: '292'
        - id: PZO1127
          pages: '292'
        - id: PZO1137
          pages: '292'
      subType: misc
    type: feat
  - _id: Ynua9MxQUUImnj50
    _key: '!actors.items!Pqgajk9PT0l5C2iE.Ynua9MxQUUImnj50'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.qe671ZP4ZsO5cbrt
      coreVersion: '12.331'
    img: icons/creatures/eyes/humanoid-single-yellow.webp
    name: Low-Light Vision
    system:
      abilityType: ex
      description:
        value: >-
          <p>A creature with low-light vision can see twice as far as a human in
          starlight, moonlight, torchlight, and similar conditions of dim light.
          It retains the ability to distinguish color and detail under these
          conditions.</p><p><em>Format:</em> low-light
          vision</p><p><em>Location:</em> Senses.</p>
      sources:
        - id: PZO1112
          pages: '301'
        - id: PZO1127
          pages: '296'
        - id: PZO1133
          pages: '295'
        - id: PZO1137
          pages: '295'
      subType: misc
    type: feat
  - _id: 1krZ0FlBKhIuVJDq
    _key: '!actors.items!Pqgajk9PT0l5C2iE.1krZ0FlBKhIuVJDq'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.R4HTn8pmyiBaMTC5
      coreVersion: '12.331'
    img: systems/pf1/icons/races/drow.png
    name: Poison
    system:
      abilityType: ex
      actions:
        - _id: nWQEJMqSqeqKAevY
          actionType: save
          activation:
            type: nonaction
            unchained:
              type: nonaction
          duration:
            units: minute
            value: '60'
          name: Use
          save:
            dc: >-
              10 + floor(@attributes.hd.total / 2) + @abilities.con.mod +
              2[racial]
            description: Fortitude negates
            type: fort
      description:
        value: >-
          <section class="secret" id="secret-sCgoWOfowByja0kp"><p>Bite - injury;
          save Fort DC 13; frequency 1/minute for 60 minutes; effect sleep for 1
          minute; cure 1 save. The save DC is Constitution-based and includes a
          +2 racial bonus.</p></section>
      showInQuickbar: true
      sources:
        - id: PZO1112
          pages: '302'
        - id: PZO1116
          pages: '300'
        - id: PZO1120
          pages: '297'
        - id: PZO1127
          pages: '297'
        - id: PZO1133
          pages: '297'
        - id: PZO1137
          pages: '296'
      subType: misc
    type: feat
name: Homunculus
prototypeToken:
  texture:
    src: icons/creatures/unholy/demon-hairy-winged-pink.webp
system:
  abilities:
    cha:
      value: 7
    con:
      value: null
    dex:
      value: 15
    str:
      value: 8
    wis:
      value: 12
  attributes:
    cmbAbility: dex
    speed:
      fly:
        base: 50
        maneuverability: good
      land:
        base: 20
  details:
    biography:
      value: >-
        <p>A homunculus is a miniature servant created by a spellcaster from his
        own blood. They are weak combatants but make effective spies,
        messengers, and scouts. A homunculus’s creator determines its precise
        features; some are more refined looking, but most creators don’t bother
        to improve the creature’s appearance beyond the minimum necessary for
        functioning.</p><p>Homunculi are little more than tools designed to
        carry out assigned tasks. They are extensions of their creators, sharing
        the same alignment and basic nature. A homunculus never willingly
        travels more than a mile from its master, though it can be removed
        forcibly. If this occurs, the creature does everything in its power to
        return to this range, as it cannot communicate with its master beyond
        this distance. An attack that destroys a homunculus deals 2d10 points of
        damage to its master. If the creature’s master is slain, the homunculus
        goes insane—it loses its Intelligence score, all feats, and all skill
        ranks, and generally claims the immediate surroundings as its domain,
        mindlessly attacking any who intrude upon its lair.</p><p>On rare
        occasions, a homunculus freed from its servitude rises above its
        master’s original intent and becomes more than a half-insane construct
        guardian of a long-forgotten lair. In some cases, a homunculus might
        even come to see itself as the rightful heir to its master’s legacy, or
        even the reincarnated spirit of the master himself.</p>
    cr:
      base: 1
    notes:
      value: <p><strong>Source</strong> <em>Pathfinder RPG Bestiary pg. 176</em></p>
  skills:
    per:
      rank: 2
    ste:
      rank: 2
  traits:
    ci:
      - deathEffects
      - disease
      - energyDrain
      - exhausted
      - fatigue
      - mindAffecting
      - paralyze
      - poison
      - sleep
      - stun
      - Necromancy
      - Fortitude saves
    di:
      - nonlethal
    languages:
      - common
    senses:
      dv:
        value: 60
      ll:
        enabled: true
    size: tiny
type: npc
