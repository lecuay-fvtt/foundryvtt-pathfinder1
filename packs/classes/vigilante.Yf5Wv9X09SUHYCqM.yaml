_id: Yf5Wv9X09SUHYCqM
_key: '!items!Yf5Wv9X09SUHYCqM'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Vigilante
system:
  armorProf:
    - lgt
    - med
    - shl
  bab: med
  classSkills:
    acr: true
    apr: true
    art: true
    blf: true
    clm: true
    crf: true
    dev: true
    dip: true
    dis: true
    esc: true
    int: true
    kdu: true
    ken: true
    klo: true
    kno: true
    lor: true
    per: true
    prf: true
    pro: true
    rid: true
    sen: true
    slt: true
    ste: true
    sur: true
    swm: true
    umd: true
  description:
    value: >-
      <p>Being a renowned hero can be dangerous to your health and prosperity.
      Fighting against corruption and the evil that lurks within society makes
      you a target and, even worse, puts your friends and family in danger. For
      those who must maintain a social persona, being a part of the greater
      community while secretly fighting against powerful forces within it
      requires leading a double life.</p><p>By day, the vigilante maneuvers
      through society, dealing with other nobles or influential individuals. By
      night, he dons a disguise and an utterly different set of goals, taking
      the fight to his foes and solving problems with a blade when words will
      not suffice.</p><p>Game Masters should consider carefully whether or not a
      vigilante will make for a good fit with their campaign. The class is one
      that requires a degree of social aptitude and roleplaying to make full use
      of its potential.</p><p>Campaigns that focus more on wilderness
      exploration, travel, or dungeon delving and that are lighter on politics,
      negotiation, and manipulation might require a vigilante player to put in
      additional effort to make full use of his class features. Alternatively, a
      vigilante is uniquely suited to make for a powerful villain, hidden by day
      behind a mask of civility and a terror at night, free to commit terrible
      acts without risking discovery.</p><p>For players, the vigilante offers a
      unique opportunity to take on the role of a character with a hidden side,
      and whose life is committed to a secret agenda that he must struggle to
      advance in a complex world. Not every problem can be solved with a dagger
      in the dark, and even the most stubborn foe might be become an ally with
      the proper bribe. For the vigilante, these tasks are both within reach as
      long as you learn to properly use your dual nature and hidden skills to
      your fullest advantage.</p><p>Life can be unfair. Think of the starving
      peasants forced to toil for the local baron or the common laborers tasked
      with building the king’s newest palace for a mere handful of copper pieces
      each week. There are those who see these injustices and do nothing. There
      are those who are willing to reap the rewards obtained through the
      suffering of others.</p><p>Then there are those who see inequality and
      find themselves driven to take action, outside the law if necessary. These
      vigilantes operate in plain sight, hiding behind respectable personas by
      day, but donning alternate guises by night to right the wrongs they see
      all around them.</p><p>Not all vigilantes are out to make the world a
      better place. Some criminals hide behind the pretense of being ordinary
      folk, only to become terrors in the shadows, stealing and killing to
      fulfill some dark agenda. In either case, the vigilante is a character of
      two natures—the face that everyone knows and the mask that inspires
      fear.</p><p><strong>Role</strong>: A vigilante can take on many tasks
      within a group. Most are skilled at negotiating delicate social situations
      and courtly intrigue, but they can also serve as stealthy spies or even
      brutish warriors in dangerous environments.</p><h2>Class Skills</h2><p>A
      vigilante’s class skills are Acrobatics (Dex), Appraise (Int), Bluff
      (Cha), Climb (Str), Craft (Int), Diplomacy (Cha), Disable Device (Dex),
      Disguise (Cha), Escape Artist (Dex), Intimidate (Cha), Knowledge
      (dungeoneering) (Int), Knowledge (engineering) (Int), Knowledge (local)
      (Int), Knowledge (nobility) (Int), Perception (Wis), Perform (Cha),
      Profession (Wis), Ride (Dex), Sense Motive (Wis), <em>Sleight of Hand</em>
      (Dex), Stealth (Dex), Survival (Wis), Swim (Str), and Use Magic Device
      (Cha).</p>
  links:
    classAssociations:
      - level: 1
        uuid: Compendium.pf1.class-abilities.Item.caHXpWVlJLxRKvOS
      - level: 1
        uuid: Compendium.pf1.class-abilities.Item.TMpfSlpC7To6uXGE
      - level: 1
        uuid: Compendium.pf1.class-abilities.Item.2uDIHsepFL6PRyRj
      - level: 1
        uuid: Compendium.pf1.class-abilities.Item.vTE94sQboMmdAakT
      - level: 2
        uuid: Compendium.pf1.class-abilities.Item.o4FeJzhEdocIO4mY
      - level: 3
        uuid: Compendium.pf1.class-abilities.Item.4U69gg3altKZvFHD
      - level: 5
        uuid: Compendium.pf1.class-abilities.Item.qCl2cw0leReIXuRn
      - level: 11
        uuid: Compendium.pf1.class-abilities.Item.2tq8HQ4ZQpiL3njr
      - level: 17
        uuid: Compendium.pf1.class-abilities.Item.fCaw2ldaQlkktvsB
      - level: 20
        uuid: Compendium.pf1.class-abilities.Item.xrruZUG1zqIHe35h
  savingThrows:
    ref:
      value: high
    will:
      value: high
  skillsPerLevel: 6
  tag: vigilante
  wealth: 5d6 * 10
  weaponProf:
    - simple
    - martial
type: class
