_id: es3RMR9PqdUJfoxn
_key: '!items!es3RMR9PqdUJfoxn'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Trompe l'Oleil
system:
  changes:
    - _id: d4pzl47v
      formula: max(0, ceil(@attributes.hd.total/4 - 1))
      target: aac
      type: untyped
    - _id: 19d9kfhn
      formula: >-
        if(gte(@attributes.hd.total, 9), 1) + if(gte(@attributes.hd.total, 17),
        1)
      target: sac
      type: untyped
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Inherited<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Trompe l’oeil creatures are
      life-sized portraits animated by powerful magic or occult phenomena. Able
      to move and talk, these constructs can also step out of their frames to
      become three-dimensional beings. Born from artistic masterpieces, trompe
      l’oeils can easily pass for their original models, though close
      examination reveals that they are not flesh and blood, but only layers of
      paint. Trompe l’oeils can be created to act as guardians and spies, or on
      occasion, a painting will animate spontaneously. Rarely, a portrait is so
      lifelike, a nascent spirit is able to inhabit it. Believing itself to be
      as good as or better than the original, such a trompe l’oeil seeks to
      eliminate and replace the painting’s subject.<p>"Trompe l’oeil" is an
      inherited template that can be added to any corporeal creature that has an
      Intelligence score (referred to hereafter as the base
      creature).<p><b>Challenge Rating:</b> Base creature’s CR +
      1.<p><b>Alignment:</b> A trompe l’oeil usually has the same alignment as
      its creator or the base creature. A trompe l’oeil that seeks to destroy
      its original model, however, has an evil alignment (but the same alignment
      on the chaotic/lawful axis).<p><b>Type:</b> The creature’s type changes to
      construct. Do not recalculate BAB, saves, or skill ranks.<p><b>Armor
      Class:</b> A trompe l’oeil gains a bonus to AC based on its HD, as noted
      in the following table. If it is depicted wearing armor or a shield, these
      items are masterwork and gain an enhancement bonus (or equivalent armor
      special abilities) when worn by the trompe l’oeil, as indicated in the
      table. If the trompe l’oeil is depicted without armor, add the armor
      enhancement bonus to its natural armor bonus instead. Armor and shields
      equipped by a trompe l’oeil melt into puddles of nonmagical paint when the
      creature is destroyed.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Trompe l'Oeil HD</b></td>

      <td><b>Armor Enhancement Bonus</b></td>

      <td><b>Shield Enhancement Bonus</b></td>

      </tr>

      <tr>

      <td>1-4</td>

      <td>—</td>

      <td>—</td>

      </tr>

      <tr>

      <td>5-8</td>

      <td>+1</td>

      <td>—</td>

      </tr>

      <tr>

      <td>9-12</td>

      <td>+2</td>

      <td>+1</td>

      </tr>

      <tr>

      <td>13-16</td>

      <td>+3</td>

      <td>+1</td>

      </tr>

      <tr>

      <td>17+</td>

      <td>+4</td>

      <td>+2</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Hit Dice:</b> Change all of the creature’s racial Hit Dice to
      d10s. All Hit Dice derived from class levels remain unchanged. As
      constructs, trompe l’oeils gain a number of additional hit points as noted
      in the following table.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Tromp l'Oeil Size</b></td>

      <td><b>Bonus Hit Points</b></td>

      </tr>

      <tr>

      <td>Tiny or smaller</td>

      <td>—</td>

      </tr>

      <tr>

      <td>Small</td>

      <td>+10</td>

      </tr>

      <tr>

      <td>Medium</td>

      <td>+20</td>

      </tr>

      <tr>

      <td>Large</td>

      <td>+30</td>

      </tr>

      <tr>

      <td>Huge</td>

      <td>+40</td>

      </tr>

      <tr>

      <td>Gargantuan</td>

      <td>+60</td>

      </tr>

      <tr>

      <td>Colossal</td>

      <td>+80</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Defensive Abilities:</b> A trompe l’oeil gains the standard
      immunities and traits of construct creatures. In addition, it gains
      rejuvenation.<p><em>Rejuvenation (Su):</em> When a trompe l’oeil is
      destroyed, it reforms 2d4 days later on its original canvas (see page
      243). The only way to permanently destroy a trompe l’oeil is to destroy
      the original canvas before the creature reforms.<p><b>Attacks:</b> A
      trompe l’oeil retains all weapon proficiencies and natural weapons. If
      it’s depicted wielding any manufactured weapons, the weapons are
      masterwork and gain an enhancement bonus (or equivalent weapon special
      abilities) when wielded by it. The bonus is based on its HD, as noted in
      the following table. A trompe l’oeil’s weapons melt into puddles of
      nonmagical paint when the creature is destroyed.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Trompe l'Oeil HD</b></td>

      <td><b>Weapon Enhancement Bonus</b></td>

      </tr>

      <tr>

      <td>1-3</td>

      <td>—</td>

      </tr>

      <tr>

      <td>4-6</td>

      <td>+1</td>

      </tr>

      <tr>

      <td>7-9</td>

      <td>+2</td>

      </tr>

      <tr>

      <td>10-12</td>

      <td>+3</td>

      </tr>

      <tr>

      <td>13-15</td>

      <td>+4</td>

      </tr>

      <tr>

      <td>16+</td>

      <td>+5</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Abilities:</b> A trompe l’oeil has no Constitution
      score.<p><b>Skills:</b> A trompe l’oeil gains a +10 racial bonus on
      Disguise checks to appear as the base creature. It also receives a +5
      bonus on Bluff checks to pretend to be the base creature and a +5 bonus on
      Stealth checks to appear as part of a painting.<p><b>Special
      Qualities:</b> A trompe l’oeil gains the following special
      qualities.<p><em>Autotelic (Ex):</em> A trompe l’oeil uses its Charisma
      score in place of its Constitution score when calculating hit points,
      Fortitude saves, and any special ability that relies on Constitution (such
      as when calculating a breath weapon’s DC).<p><em>Enter Painting (Su):</em>
      As a standard action, a trompe l’oeil can enter a painting it touches.
      When it does so, its physical body disappears, and its image appears in
      the painting. The trompe l’oeil can use its normal senses and attempt
      Perception checks to notice anything occurring near the painting. While
      within a painting, the trompe l’oeil can talk and move anywhere within the
      picture or even temporarily alter it (such as by picking a flower in the
      painting). It cannot use any spells or other abilities while within an
      image. In addition, the trompe l’oeil gains the freeze universal monster
      ability to appear as part of the painting. The trompe l’oeil can leave the
      painting as a move action. Once it leaves the painting, the image
      immediately reverts to the appearance it had before the trompe l’oeil
      entered. If someone destroys or damages the painting, the trompe l’oeil is
      unharmed, but exits the image.</p>
  subType: template
type: feat
