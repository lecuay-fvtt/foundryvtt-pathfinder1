_id: gl524v9qd5jji2ex
_key: '!items!gl524v9qd5jji2ex'
_stats:
  coreVersion: '12.331'
folder: NE0tEwSvcr71GnPC
img: systems/pf1/icons/misc/magic-swirl.png
name: Contact Entity I
system:
  actions:
    - _id: it3ellqau3bsxszq
      activation:
        type: minute
        unchained:
          type: minute
      duration:
        units: inst
      name: Use
      range:
        units: mi
        value: '100'
      target:
        value: up to 20 entities of 6 HD or fewer; see text
  components:
    material: true
    somatic: true
    verbal: true
  description:
    value: >-
      <p>You send out a magical message to any eldritch entities of a particular
      kind within a 100-mile radius, which can be delivered to up to 20 such
      creatures, starting with the nearest creatures until the limit has been
      met. This spell can’t contact creatures with more than 6 Hit Dice. You
      can’t send a specific message, but this spell (and all similar contact
      entity spells) can be characterized as an open invitation to make contact
      and establish communication. If there is an appropriate entity within
      range, the spell succeeds automatically. You don’t know whether the
      message was received, nor any specific details about what creatures
      received it or how many. Creatures that receive the message know the
      location and distance from where the spell was cast. Because this spell
      doesn’t call or summon the target, the target must have its own way to
      reach the place where the spell was cast.</p><p>How creatures respond to a
      contact spell is circumstantial and it is possible the creatures will
      simply ignore the spell. Creatures that come and investigate do so in
      their own time. They usually arrive cautiously, aware of the potential for
      ambush. Targets of the spell might inform their organization or community
      if they have one. There are no restrictions on how the creatures react to
      being contacted, and they might respond with hostility, parley, entertain
      an alliance, or subjugate the caster and their related community. Using
      this spell counts as mentally contacting the creature for the purpose of
      any of its special abilities (such as the star-spawn’s overwhelming mind).
      For the purpose of spells like scrying, the creature has firsthand
      knowledge of you and a connection similar to if it possessed a likeness of
      you.</p><p>Each type of creature requires a different material component
      that must be included when casting the spell, as shown on Table 4–1:
      Contact Entity on page 112. Some of these components are expensive or
      might require quests to acquire. Contacting certain types of creatures
      makes the spell chaotic, evil, or both, as indicated on the
      table.</p><table><thead><tr><th><p></p></th><th><p>Material
      Component</p></th></tr></thead><tbody><tr><td><p>Cerebric
      fungus<sup>B3</sup></p></td><td><p>Scholarly
      tome</p></td></tr><tr><td><p>Deep one<sup>B5</sup></p></td><td><p>Engraved
      stone tablet</p></td></tr><tr><td><p>Grioth<sup>B5</sup></p></td><td><p>An
      eye struck blind during an eclipse
      [evil]</p></td></tr><tr><td><p>Nightgaunt<sup>B4</sup></p></td><td><p>Sculpture
      of an animal with the face filed smooth
      [chaotic]</p></td></tr><tr><td><p>Ratling<sup>B4</sup></p></td><td><p>Spell
      scroll of 1st level or higher [chaotic,
      evil]</p></td></tr><tr><td><p>Voonith<sup>B3</sup></p></td><td><p>Bundle
      of cattail plants [chaotic]</p></td></tr></tbody></table>
  learnedAt:
    class:
      arcanist: 2
      cleric: 2
      medium: 1
      occultist: 2
      oracle: 2
      psychic: 2
      shaman: 2
      sorcerer: 2
      summoner: 2
      summonerUnchained: 2
      warpriest: 2
      witch: 2
      wizard: 2
  level: 2
  materials:
    value: see text
  school: evo
  sources:
    - id: PZO1135
      pages: '112'
  sr: false
type: spell
