_id: 72r8mMxzMNWpdt0D
_key: '!items!72r8mMxzMNWpdt0D'
_stats:
  coreVersion: '12.331'
img: icons/magic/light/hand-sparks-smoke-green.webp
name: Oracle Spells
system:
  associations:
    classes:
      - Oracle
  description:
    value: >-
      <p>An oracle casts divine spells drawn from the cleric spell lists. She
      can cast any spell she knows without preparing it ahead of time. To learn
      or cast a spell, an oracle must have a Charisma score equal to at least 10
      + the spell level. The Difficulty Class for a saving throw against an
      oracle's spell is 10 + the spell's level + the oracle's Charisma
      modifier.</p><p>Like other spellcasters, an oracle can cast only a certain
      number of spells per day of each spell level. Her base daily spell
      allotment is given on
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.8HNFlPoMR5NxGMzD#class-features$7]{Table:
      Oracle}. In addition, she receives bonus spells per day if she has a high
      Charisma score (see
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.BfKeMBHQruwqyUXX]{Table:
      Ability Modifiers and Bonus Spells}).</p><p>Unlike other divine
      spellcasters, an oracle's selection of spells is extremely limited. An
      oracle begins play knowing four 0-level spells and two 1st-level spells of
      her choice. At each new oracle level, she gains one or more new spells, as
      indicated on
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.8HNFlPoMR5NxGMzD#spells-known$6]{Table:
      Spells Known}. Unlike spells per day, the number of spells an oracle knows
      is not affected by her Charisma score; the numbers on Table: Spells Known
      are fixed.</p><p>In addition to the spells gained by oracles as they gain
      levels, each oracle also adds all of either the cure spells or the inflict
      spells to her list of spells known (cure spells include all spells with
      "cure" in the name, inflict spells include all spells with "inflict" in
      the name). These spells are added as soon as the oracle is capable of
      casting them. This choice is made when the oracle gains her first level
      and cannot be changed.</p><p>Unlike a cleric, an oracle need not prepare
      her spells in advance. She can cast any spell she knows at any time,
      assuming she has not yet used up her spells per day for that spell level.
      Oracles do not need to provide a divine focus to cast spells that list
      divine focus (DF) as part of the components.</p>
  sources:
    - id: PZO1115
      pages: '42'
  subType: classFeat
type: feat
