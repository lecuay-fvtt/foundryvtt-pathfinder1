export {};

declare module "./action.mjs" {
  interface ItemAction {
    /** @internal */
    _id: string;
    img?: string;
    description: string;
    tag?: string;
    activation: {
      cost: number;
      type: string;
      unchained: {
        cost: number;
        type: string;
      };
    };
    duration: {
      value: string;
      units: string;
      dismiss: boolean;
      concentration: boolean;
    };
    target: {
      value: string;
    };
    range: {
      value: string;
      units: string;
      maxIncrements: number;
      minValue: string;
      minUnits: string;
    };
    uses: {
      autoDeductChargesCost: string;
      perAttack: boolean;

      self: {
        value: number;
        maxFormula: string;
        per: string;
      };
    };
    measureTemplate: {
      type: string;
      size: string;
      color: string;
      texture: string;
    };
    bab: string;
    attackName: string;
    actionType: string;
    attackBonus: string;
    damage: {
      parts: pf1.models.action.DamagePartModel[];
      critParts: pf1.models.action.DamagePartModel[];
      nonCritParts: pf1.models.action.DamagePartModel[];
    };
    extraAttacks: {
      type: string;
      manual: pf1.models.action.ExtraAttackModel[];
      formula: {
        count: string;
        bonus: string;
        label: string;
      };
    };
    ability: {
      attack: string;
      damage: string;
      max?: number;
      damageMult?: number;
      critRange?: number;
      critMult?: number;
    };
    save: {
      dc: string;
      type: string;
      description: string;
      harmless: boolean;
    };
    notes: {
      effect: string[];
      footer: string[];
    };
    soundEffect: string;
    powerAttack: {
      multiplier: number;
      damageBonus: number;
      critMultiplier: number;
    };
    naturalAttack: {
      primary: boolean;
      secondary: {
        attackBonus: string;
        damageMult: number;
      };
    };
    held: string;
    nonlethal: boolean;
    splash: boolean;
    touch: boolean;
    ammo: {
      type: string;
      cost: number;
    };
    effect: string;
    area: string;
    conditionals: Collection<pf1.components.ItemConditional>;
    enh: {
      value?: number;
    };
    material: {
      normal: {
        value: string;
        custom: boolean;
      };
      addon: Set<String>;
    };
    alignments: {
      lawful: boolean;
      chaotic: boolean;
      good: boolean;
      evil: boolean;
    };
    parent?: ItemPF;
  }
}
