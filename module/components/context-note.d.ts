export {};

declare module "./context-note.mjs" {
  interface ContextNote {
    text: string;
    target: string;
  }
}
