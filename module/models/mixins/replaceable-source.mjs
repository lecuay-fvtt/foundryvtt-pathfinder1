/**
 * Replaceable Source Mixin
 *
 * Adds function to the data model to replace source data.
 *
 * @see {@link https://github.com/foundryvtt/foundryvtt/issues/11792}
 *
 * @param {T} Base - Base datamodel
 * @returns {T} - Enriched class
 */
export const ReplaceableSourceMixin = (Base) =>
  class extends Base {
    /**
     * Replace datamodel source
     *
     * @param {object} source - New source
     * @returns {object} - Result of DataModel.updateSource()
     */
    replaceSource(source) {
      source = foundry.utils.deepClone(source); // Prevent mutations to source data

      this.constructor.migrateData(source);

      // Clear values not present in new data
      // Does not do deep comparisons
      for (const [key, schema] of Object.entries(this.schema.fields)) {
        if (schema.readonly) continue;
        if (key in source) continue;
        if (key in this._source) {
          source[key] = undefined; // Delete syntax does not work here
        }
      }

      return this.updateSource(source); // recursive:false,diff:false does not work here
    }
  };
