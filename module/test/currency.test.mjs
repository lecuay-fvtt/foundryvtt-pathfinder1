const mergings = [
  [{ pp: 10, gp: 50, sp: 500, cp: 3000 }, "gp", 230],
  [{ pp: 1, gp: 20, sp: 330, cp: 4 }, "pp", 6.304],
];

const conversions = [
  [1234, "cp", { cp: 1234 }],
  [1234, "sp", { sp: 123, cp: 4 }],
  [6304, "pp", { pp: 6, gp: 3, cp: 4 }],
];

const splits = [
  [1234, { gp: 12, sp: 3, cp: 4 }],
  [43218, { gp: 432, sp: 1, cp: 8 }],
];

// Pretty print simple object
const objStr = (obj) => {
  const parts = Object.entries(obj)
    .filter(([_, value]) => value != 0)
    .map(([key, value]) => `${key}: ${value}`);

  return "{ " + parts.join(", ") + " }";
};

/**
 * Currency tests
 */
export function registerCurrencyTests() {
  quench.registerBatch(
    "pf1.utils.currency",
    (context) => {
      const { describe, it, expect, before, after } = context;

      describe("merge", function () {
        for (const [input, type, output] of mergings) {
          it(`${objStr(input)} = ${output}`, function () {
            expect(pf1.utils.currency.merge(input, type)).to.equal(output);
          });
        }
      });

      describe("convert", function () {
        for (const [input, type, output] of conversions) {
          it(`${input} = ${objStr(output)}`, function () {
            expect(pf1.utils.currency.convert(input, type, { pad: false })).to.deep.equal(output);
          });
        }
      });

      describe("split", function () {
        for (const [input, output] of splits) {
          it(`${input} = ${objStr(output)}`, function () {
            expect(pf1.utils.currency.split(input, { pad: false })).to.deep.equal(output);
          });
        }
      });

      const cc = pf1.config.currency;
      const currency = { cp: 500, gp: 2, pp: 10, sp: 7 };
      const actor = new Actor.implementation({
        type: "character",
        name: "currency test",
        system: { currency: { ...currency } },
      });

      describe("actor", function () {
        it("base rates", () => {
          const baseCp = actor.getTotalCurrency();
          expect(baseCp).to.equal(10770);

          const baseGp = actor.getTotalCurrency({ inLowestDenomination: false });
          expect(baseGp).to.equal(107.7);
        });

        describe("custom (pp*0.8, gp*1.2, sp*2.5)", () => {
          let backup;
          before(() => {
            backup = foundry.utils.deepClone(cc);
            // Customize currency rates
            cc.rate = { pp: 800, gp: 120, sp: 25 };
            cc.lowest = "cp";
            cc.standard = "gp";
          });

          after(() => {
            cc.rate = backup.rate;
            cc.lowest = backup.lowest;
            cc.standard = backup.standard;
          });

          it("copper", () => {
            const customCp = actor.getTotalCurrency();
            expect(customCp).to.equal(
              currency.cp + currency.sp * cc.rate.sp + currency.gp * cc.rate.gp + currency.pp * cc.rate.pp
            );
          });
          it("gold", () => {
            const customGp = actor.getTotalCurrency({ inLowestDenomination: false });
            expect(customGp).to.equal(
              (currency.cp + currency.sp * cc.rate.sp + currency.gp * cc.rate.gp + currency.pp * cc.rate.pp) /
                cc.rate.gp
            );
          });
        });
      });
    },
    {
      displayName: "PF1: Currency",
    }
  );
}
