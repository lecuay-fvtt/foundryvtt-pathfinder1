const getMessage = (html) => game.messages.get(html.dataset.messageId);
const isOwnedSpellCard = (msg) => {
  const item = msg.itemSource;
  return item && item.type === "spell" && item.isOwner && !!item.actor;
};

/**
 * @param _
 * @param {object[]} entries - Context menu entries
 */
function spellCardContextMenu(_, entries) {
  entries.unshift(
    {
      name: "PF1.Chat.FocusReference",
      id: "pf1-focus-reference", // made up ID field so modules can recognize this
      icon: '<i class="fa-solid fa-magnifying-glass context-icon pf1 focus-reference"></i>',
      condition: ([html]) => fromUuidSync(getMessage(html)?.system.reference) instanceof ChatMessage,
      callback: ([html]) => {
        /** @type {ChatMessage} */
        const msg = fromUuidSync(getMessage(html).system.reference);
        /** @type {HTMLElement} */
        const log = html.closest(".chat-message[data-message-id]").parentElement;
        log.querySelector(`.chat-message[data-message-id='${msg.id}']`).scrollIntoView({ block: "start" });
      },
    },
    {
      name: "PF1.ConcentrationCheck",
      id: "pf1-roll-concentration",
      icon: '<i class="fa-solid fa-brain context-icon pf1 concentration"></i>',
      condition: ([html]) => isOwnedSpellCard(getMessage(html)),
      callback: ([html]) => {
        const msg = getMessage(html);
        const item = msg.itemSource;
        const actor = item?.actor;
        actor.rollConcentration(item.system.spellbook, { messageId: msg.id, reference: msg.uuid });
      },
    },
    {
      name: "PF1.CasterLevelCheck",
      id: "pf1-roll-caster-level",
      icon: '<i class="fa-solid fa-wand-magic-sparkles context-icon pf1 caster-level"></i>',
      condition: ([html]) => isOwnedSpellCard(getMessage(html)),
      callback: ([html]) => {
        const msg = getMessage(html);
        const item = msg.itemSource;
        const actor = item?.actor;
        actor.rollCL(item.system.spellbook, { messageId: msg.id, reference: msg.uuid });
      },
    }
    // TODO: Roll ASF option
  );
}

Hooks.on("getChatLogEntryContext", spellCardContextMenu);
