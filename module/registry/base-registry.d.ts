export {};

declare module "./base-registry.mjs" {
  interface RegistryEntry {
    _id: string;
    name: string;
    flags?: object;
    namespace: string;
  }
}
