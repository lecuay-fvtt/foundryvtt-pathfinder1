import { ItemPhysicalPF } from "./item-physical.mjs";

/**
 * Consumable item
 *
 * For potions, wands, scrolls, drugs, etc.
 */
export class ItemConsumablePF extends ItemPhysicalPF {
  /** @inheritDoc */
  static system = Object.freeze({ ...super.system, subtypeName: true });

  /**
   * @inheritDoc
   */
  adjustContained() {
    super.adjustContained();

    this.system.carried = true;
  }

  /**
   * @inheritDoc
   */
  getLabels({ actionId, rollData } = {}) {
    const labels = super.getLabels({ actionId, rollData });

    labels.subType = pf1.config.consumableTypes[this.subType];

    return labels;
  }
}
